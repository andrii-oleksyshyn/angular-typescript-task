namespace Products {

    interface IProduct {
        ProductID: number;
        ProductName: string;
        SupplierID: number;
        CategoryID: number;
        QuantityPerUnit: string;
        UnitPrice: string;
        UnitsInStock: number;
        UnitsOnOrder: number;
        ReorderLevel: number;
        Discontinued: boolean;
    }

    interface IProductResponse {
        'odata.metadata': string;
        'odata.nextLink': string;
        value: IProduct[];
    }

    class ProductsController {
        public apiUrl: string = '//services.odata.org/V3/Northwind/Northwind.svc/Products/';
        public tableData: IProduct[] = null;

        static $inject: Array<string> = ['$http'];

        constructor(public $http: angular.IHttpService) {}

        public getProducts(): void {
            this.$http.get(this.apiUrl)
                .then((res: {data: IProductResponse}) => {
                    this.tableData = res.data.value;
                }, (err) => {
                    console.log(err);
                });
        }

        public resetTableData(): void {
            this.tableData = null;
        }
    }

    angular
        .module('app')
        .component('products', {
            templateUrl: 'app/products/products.html',
            controller: ProductsController
        });
}
